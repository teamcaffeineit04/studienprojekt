import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CoffeeList} from '../models/coffeelist';
import {BehaviorSubject, Observable} from 'rxjs';
import {UrlRequestComponent} from '../components/url-request/url-request.component';
import { environment } from '../../environments/environment';

//Für das Updaten der Daten über HTTP PUT REQUEST
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}


@Injectable({
  providedIn: 'root'
})
export class CoffeeserviceService {
  urlString: string;

  coffeeURL:string = environment.protocol + environment.apiUrl + '/coffee'; //'https://coffeeList.azurewebsites.net/coffee';


  constructor(private http:HttpClient) {

  }

  getCoffees():Observable<CoffeeList[]>{
    return this.http.get<CoffeeList[]>(this.coffeeURL);
  }

  deleteCoffee(coffeelist:CoffeeList):Observable<CoffeeList>{
    const url = `${this.coffeeURL}/${coffeelist.id}`;
    return this.http.delete<CoffeeList>(url,httpOptions);
  }

  addCoffee(coffeelist:CoffeeList):Observable<CoffeeList>{
    return this.http.post<CoffeeList>(this.coffeeURL, coffeelist, httpOptions);
  }


  public saveUrl(urlRequest){
    this.urlString = urlRequest;
    console.log(environment.protocol + this.urlString + '/coffee');    //console.log('https://'+ this.urlString+'/coffee');
    this.coffeeURL = environment.protocol + this.urlString + '/coffee'    //this.coffeeURL = 'https://'+ this.urlString+'/coffee';
  }


}
