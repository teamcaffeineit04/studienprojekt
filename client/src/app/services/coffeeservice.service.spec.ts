import { TestBed } from '@angular/core/testing';

import { CoffeeserviceService } from './coffeeservice.service';

describe('CoffeeserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoffeeserviceService = TestBed.get(CoffeeserviceService);
    expect(service).toBeTruthy();
  });
});
