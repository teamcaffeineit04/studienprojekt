import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {CoffeeList} from '../../models/coffeelist';
import { CoffeeserviceService} from '../../services/coffeeservice.service';

@Component({
  selector: 'app-coffee-list-item',
  templateUrl: './coffee-list-item.component.html',
  styleUrls: ['./coffee-list-item.component.css']
})
export class CoffeeListItemComponent implements OnInit {
  @Input() coffeelist: CoffeeList;
  @Output() deleteCoffee: EventEmitter<CoffeeList> = new EventEmitter();

  constructor(private coffeeService:CoffeeserviceService) { }

  ngOnInit() {

  }

  //Set Dynamic Classes
  setClasses(){
    let classes = {
      coffee: true
    }
    return classes;
  }

  onDelete(coffeelist){
    this.deleteCoffee.emit(coffeelist)
  }

}
