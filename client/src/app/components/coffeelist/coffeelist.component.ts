import { Component, OnInit,Input, ChangeDetectionStrategy,DoCheck,OnChanges } from '@angular/core';
import { CoffeeList } from '../../models/coffeelist';
import {CoffeeserviceService} from '../../services/coffeeservice.service'

@Component({
  selector: 'app-coffeelist',
  templateUrl: './coffeelist.component.html',
  styleUrls: ['./coffeelist.component.css']
})
export class CoffeelistComponent implements OnInit {
  @Input() coffeelists: CoffeeList[];

  constructor(private coffeeService:CoffeeserviceService) {
  }

  ngOnInit() {
    this.coffeeService.getCoffees().subscribe(coffeelists=>{
      this.coffeelists = coffeelists;
    });
  }

  public getCoffeList(){
    this.coffeeService.getCoffees().subscribe(coffeelists=>{
      this.coffeelists = coffeelists;
    });
  }


  deleteCoffee(coffeelist:CoffeeList){
    //Lösche von UI
    this.coffeelists = this.coffeelists.filter(t =>t.id !== coffeelist.id);

    //Lösche vom Server
    //Sende ein POST REQUEST für die DB, wird im coffeeservice.service.ts aufgerufen
    this.coffeeService.deleteCoffee(coffeelist).subscribe();
  }

  addCoffee(coffeelist:CoffeeList){
    this.coffeeService.addCoffee(coffeelist).subscribe(coffeelist => {
      this.coffeelists.push(coffeelist);
    })
  }


}
