import { Component, OnInit } from '@angular/core';
import {CoffeeserviceService} from '../../services/coffeeservice.service'
import { CoffeelistComponent} from '../coffeelist/coffeelist.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-url-request',
  templateUrl: './url-request.component.html',
  styleUrls: ['./url-request.component.css']
})
export class UrlRequestComponent implements OnInit {

  urlRequest: string = environment.apiUrl;

  constructor(private coffeeService: CoffeeserviceService,private coffee: CoffeelistComponent) { }

  ngOnInit() {

  }

  onSave(urlRequest){
    urlRequest: this.urlRequest;
    this.coffeeService.saveUrl(urlRequest);
    this.coffee.getCoffeList();
  }


}
