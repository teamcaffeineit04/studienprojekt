import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlRequestComponent } from './url-request.component';

describe('UrlRequestComponent', () => {
  let component: UrlRequestComponent;
  let fixture: ComponentFixture<UrlRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
