import { Component, OnInit, EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-add-coffee',
  templateUrl: './add-coffee.component.html',
  styleUrls: ['./add-coffee.component.css']
})
export class AddCoffeeComponent implements OnInit {
  @Output() addCoffee: EventEmitter<any> = new EventEmitter();
  name: string;
  menge: number;

  constructor() { }

  ngOnInit() {

  }

  onSubmit(){
    const coffee = {
      name: this.name,
      menge: this.menge
    }

    this.addCoffee.emit(coffee);
  }

}
