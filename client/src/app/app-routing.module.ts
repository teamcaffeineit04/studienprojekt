import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoffeelistComponent } from './components/coffeelist/coffeelist.component';
import { AboutComponent} from './components/pages/about/about.component';

const routes: Routes = [
  {
    path: '', component: CoffeelistComponent
  },
  {
    path: 'about', component: AboutComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
