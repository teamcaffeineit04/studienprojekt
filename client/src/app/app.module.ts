import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoffeelistComponent } from './components/coffeelist/coffeelist.component';
import { CoffeeListItemComponent } from './components/coffee-list-item/coffee-list-item.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { AddCoffeeComponent } from './components/add-coffee/add-coffee.component';
import { FormsModule} from '@angular/forms';
import { AboutComponent } from './components/pages/about/about.component';
import { UrlRequestComponent } from './components/url-request/url-request.component';

@NgModule({
  declarations: [
    AppComponent,
    CoffeelistComponent,
    CoffeeListItemComponent,
    HeaderComponent,
    AddCoffeeComponent,
    AboutComponent,
    UrlRequestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
