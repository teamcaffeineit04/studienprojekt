# HOW TO AZURE FUNCTIONS MIT JAVA AM BEISPIEL EINER SAMPLE ANWENDUNG

## 1. System einrichten

### 1.1. Einrichten der Java OpenJDK von (Azul)

#### 1.1.1. Download und entpacken

* Download der [JDK](https://cdn.azul.com/zulu/bin/zulu8.38.0.13-ca-jdk8.0.212-win_x64.zip)

* Entpacke den Ordner __*zulu8.38.0.13-ca-jdk8.0.212-win_x64“*__ nach __*C:\Program Files\Java*__

#### 1.1.2. Einrichten der Umgebungsvariablen

* Drücke hierzu die __*Window-*__ und die __*R-Taste*__ gleichzeitig. 

* Gebe __*sysdm.cpl*__ ein und bestätige mit __*Enter*__ oder dem drücken auf die *__OK-Taste__*. 

* Wähle nun in dem sich geöffneten Fenster der Systemeigenschaften den Reiter __*Erweitert*__ aus und klicke anschließend auf den Knopf mit der Beschriftung __*Umgebungsvariablen…*__, der sich im unteren Teil des Fensters befindet. 

#### 1.1.3. JAVA_HOME setzen

* Innerhalb des nun erschienen Fensters __*Umgebungsvariablen*__ klicke bei den __Systemvariablen__ den __*Neu…-Knopf*__.

* Tippe in dem sich geöffneten Fenster __*Neue SystemVariable*__ in das Feld __*Name der Variablen:*__ __JAVA_HOME__ ein und unter *__Wert der Variablen__* __*C:\Program Files\Java\zulu8.38.0.13-ca-jdk8.0.212-win_x64*__ ein und bestätige mit der __*OK-Taste*__.

#### 1.1.4. Bearbeiten der Path Variablen

*  Klicke bei den __Systemvariablen__ im unteren Teil des Fensters die Variable __*Path*__ an und anschließend auf den __*Bearbeiten…*__-Knopf. 

* Hier setzt du nun den Eintrag für die JDK indem du auf __*Neu*__ drückst und Anschließend __*%JAVA_HOME%\bin*__ eintippst und mithilfe des __*OK*__-Knopfs das Fenster schließt.

* Betätige nun in allen Geöffneten Fenstern die __*OK-Taste*__.

#### 1.1.5. Test der Einstellungen

* Drücke erneut die __*Window*__- und die __*R*__-Taste gleichzeitig und tippe __*cmd*__ ein und führe diese Anweisung aus.

* In der nun erschienen Eingabeaufforderung gebe __*``` java -version ```*__  ein und bestätige mit Enter. Folgende Ausgabe sollte erscheinen. ![java_version](./README/pictures/java_version.png)

* Als nächstes Überprüfen wir die __JAVA_HOME__ Varible Indem wir __*```echo %JAVA_HOME%```*__ eingeben. ![java_home](./README/pictures/java_home.png)

### 1.2. Maven einrichten

#### 1.2.1. Download und entpacken
* Download von [Maven](http://mirrors.ae-online.de/apache/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.zip)

* Entpacke nun den Ordner __*apache-maven-3.6.1*__ nach __*C:\Program Files\Maven*__. 

#### 1.2.2. Bearbeiten der Path Variblen
	
* Wiederhole die Schritte aus __*1.1.2*__ und __*1.1.4*__ setze jedoch __*C:\Program Files\Maven\apache-maven-3.6.1\bin*__ als Wert in der __*Path*__-Variablen.

* Schließe wieder alle Fernster über die __*OK*__-Taste.

#### 1.2.3. Test der Einstellungen

* Öffne erneut die Eingabeaufforderung und gebe __*```mvn -version```*__ ein. 

* Folgende Ausgabe sollte erscheinen ![mvn_version](./README/pictures/mvn_version.png)

### 1.3. NodeJS einrichten

#### 1.3.1. Download und Installation 

* Herunterladen des [Installer](https://nodejs.org/dist/v10.16.0/node-v10.16.0-x64.msi).

* Installer ausführen und den Anweisungen folgen.

#### 1.3.2. Test der Installation

* Innerhalb der Eingabeaufforderung __*```npm -version```*__ eintippen, was zu folgender Ausgabe führen sollte:  
![npm_version](./README/pictures/npm_version.png)

### 1.4. Git einrichten

#### 1.4.1. Download und Installation

* [Installer](https://github.com/git-for-windows/git/releases/download/v2.22.0.windows.1/Git-2.22.0-64-bit.exe) Downloaden.

* Anweisungen des Installer folgen.

#### 1.4.2. Test der Installation
	
* Eingabe von __*```git --version```*__ innerhalb der Eingabeaufforderung.

* Es sollte folgende Ausgabe erscheinen:
 ![git_version](./README/pictures/git_version.png)

### 1.5. Azure CLI

#### 1.5.1. Download und Installation

* [Installer](https://aka.ms/installazurecliwindows) herunterladen und ausführen.

* Anweisungen folgen.

#### 1.5.2. Test der Installation

* Eingabe von __*```az --version```*__ sollte zu folgender Ausgabe ![azure_version](./README/pictures/azure_version.png) innerhalb  der Eingabeaufforderung führen.

### 1.6. Visual Studio Code (VS Code)

#### 1.6.1. Download und Installation

* [Herunterladen](https://aka.ms/win32-x64-user-stable).

* Installation über den Installer.

* Falls noch nicht durch den Installer veranlasst VS Code ausführen.

#### 1.6.2. Java Extensions einrichten

* Innerhalb von VS Code drücke __*strg*__-,__*shift*__- und die *__X__*-Taste gleichzeitig und tippe __*Java exte*__

* Installiere die Erweiterung indem du auf den grünen __*Install*__-Knopf drückst. ![vscode_extension1](./README/pictures/vscode_extension1.png)  

#### 1.6.3. Azure Functions Extension einrichten

* Auf die gleiche Weise, wie in vorhergehenden Schritt __*(1.6.2.)*__ installierst du nun die __Azure Functions Extension__, indem du nach __*azure functions*__ suchst und auch hier den __Install__-Knopf betätigst.

#### 1.6.4. Installation der Azure Functions Core  Tools
	
* Öffne über gleichzeitiges drücken der Tasten __*strg*__, __*shift*__ und __*Ö*__ ein Terminal innerhalb von VS Code.

* Gebe im Terminal  __*```npm i -g azure-functions-core-tools@core --unsafe-perm true```*__ ein und bestätige mit __*Enter*__.

---

## 2. Azure Functions

### 2.1. Erstellen eines neuen Projekts
	
* Drücke die __*strg*__-, __*shift*__- und __*P*__- Taste gleichzeitig innerhalb von VS Code.

* Tippe __*Azure Functions: Create New Project*__ ein und bestätige mit __Enter__.

* Erstelle einen beliebigen Ordner und wähle diesen im Auswahldialog aus.

* Nun tippe __*Java*__ ein und bestätige mit der __Enter__-Taste.

* Im nächsten Schritt musst du eine __group id__, wie bei Java Projekten üblich festlegen. Wir wählen hierfür __*dev.teamcaffeine*__ und bestätige die Eingabe.

* Nun wirst du aufgefordert eine __artifact id__ einzugeben, diese definiert den Namen unserer Anwendung also tippe __*coffeeList*__ ein und bestätige erneut deine Eingabe.

* Nun legst du die __*Versionsnummer*__ fest wähle hier am besten __*1.0*__.

* Als nächstes müssen wir einen __package name__ angeben. Hier übernehmen wir den bereits vorgeschlagenen, also __*dev.teamcaffeine*__.

* Als __App Name__ verwenden wir den bereits vorgeschlagenen Namen, da der vorgeschlagene Name über den im Namen enthaltenen Zeitstempel auf jeden Fall einzigartig sein sollte. Dies ist wichtig, da wir über diesen Namen später unsere deployten Functions als Teil einer URL von Azure aufrufen werden.

* Den darauffolgenden VS Code Dialog können wir einfach mit __Enter__ bestätigen.

### 2.2. Die erste Function
	
* Als erstes entfernen wir die erstellte Datei __*Function.java*__ im __main__ Verzeichnis als auch die Datei __*FunctionTest.java*__ im __test__ Verzeichnis unseres Projektes.

* Sobald dies geschehen ist erstellen wir eine neue Datei __*HalloTeamCaffeine.java*__ und befüllen sie mit folgendem Code:

``` java
package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

public class HalloTeamCaffeine {

    @FunctionName("hallo-from-TeamCaffeine")
    public HttpResponseMessage hallofromTeamCaffeine(@HttpTrigger(name = "req", methods = {HttpMethod.GET}, authLevel = AuthorizationLevel.ANONYMOUS) HttpRequestMessage<Optional<String>> 
 		request, final ExecutionContext context) {

        context.getLogger().info("Aufruf der hallo-from-TeamCaffeine function");
        
        return request.createResponseBuilder(HttpStatus.OK).body("Hello! We are TeamCaffeine").build();
    
    }

}
```

* Als nächstes bearbeiten wir noch die Datei __*local.settings.json*__, so dass sie wie folgt aussieht:

``` javascript
{
  "IsEncrypted": false,
  "Values": {
    "AzureWebJobsStorage": "",
    "FUNCTIONS_WORKER_RUNTIME": "java"
  },
  "Host": {
    "LocalHttpPort": 1337,
    "CORS": "*",
  }
}
```

* Sowie die Datei __*host.json*__:   

``` javascript
{
  "version": "2.0",
  "extensions": {
    "http": {
      "routePrefix": ""
    }
  }
}
```
* Zum Ausführen unserer Function öffnen wir nun ein Terminal innerhalb von VS Code (__*strg*__-, __*shift*__- und __*Ö*__-Taste) und führen den Befehl __*```mvn clean package azure-functions:run```*__ aus.

* Wir können nun über einen Browser unter der URL __*http://localhost:1337/hallo-from-TeamCaffeine*__  unsere Function aufrufen.

* Um die Ausführung der Function zu beenden, drücke innerhalb des Terminals gleichzeitig die __*strg*__- und __*C*__-Taste.

---

## 3. Die Sample Anwendung

### 3.1. Abhängigkeiten einpflegen

* Öffne die Datei „pom.xml“ und füge die folgende Abhängigkeiten, für Hibernate und die Datenbank H2, unter ```<dependencies>``` ein.

``` xml
<dependency>
    <groupId>org.hibernate</groupId>
      	<artifactId>hibernate-core</artifactId>
      	<version>5.3.0.Final</version>
</dependency>
<dependency>
    <groupId>org.hibernate</groupId>
      <artifactId>hibernate-entitymanager</artifactId>
      <version>5.3.0.Final</version>
</dependency>
<dependency>
<groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <version>1.4.199</version>
</dependency>
```

### 3.2. Coffee Model

* Erstelle einen neuen Ordner __*model*__ im Verzeichnis __teamcaffeine__ des Projekts.

* Lege nun in diesem Ordner die Datei __*Coffee.java*__ mit folgendem inhalt an:

``` java
package dev.teamcaffeine.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Coffee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String name;
    int menge;

    public Coffee() {}

    public Coffee(String name, int menge) {

        this.id = 0;
        this.name = name;
        this.menge = menge;

    }

    public void setId(int id) {

        this.id = id;

    }

    public void setMenge(int menge) {

        this.menge = menge;

    }

    public void setName(String name) {

        this.name = name;

    }

    public int getId() {

        return this.id;

    }

    public String getName() {

        return this.name;

    }

    public int getMenge() {

        return this.menge;

    }

}
```

### 3.3. Erstellen des Persistance-Layers

* Erstelle nun die Verzeichnisstruktur __*resources/META-INF*__ innerhalb des __main__ Ordners deines Projekts und erstelle die Datei __*persistence.xml*__ mit folgendem Inhalt:

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<persistence version="2.1"
    xmlns="http://xmlns.jcp.org/xml/ns/persistence" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_1.xsd">
    <persistence-unit name="coffee-H2" transaction-type="RESOURCE_LOCAL">
        <class>dev.teamcaffeine.model.Coffee</class>
        <properties>
            <property name="javax.persistence.jdbc.driver" value="org.h2.Driver" />
            <property name="javax.persistence.jdbc.url"    value="jdbc:h2:~/coffee" />
            <property name="javax.persistence.jdbc.user" value="sa" />
            <property name="javax.persistence.jdbc.password" value="" />
            <property name="hibernate.hbm2ddl.auto" value="update" />
        </properties>
    </persistence-unit>
</persistence>

```
* Als nächstes wird ein weiterer Ordner __*dao*__ innerhalb des Ordners __teamcaffeine__ angelegt und die Datei __*CoffeeDAO.java*__ mit folgendem Inhalt erstellt:

```java
package dev.teamcaffeine.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dev.teamcaffeine.model.Coffee;

public class CoffeeDAO {

    private EntityManager entityManager;

    public CoffeeDAO() {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("coffee-H2");
        this.entityManager = entityManagerFactory.createEntityManager();
        
    }

    public void saveCoffee(Coffee coffee) {

        this.entityManager.getTransaction().begin();
        this.entityManager.persist(coffee);
        this.entityManager.getTransaction().commit();

    }

    public void updateCoffee(int id, String name, int menge) {

        Coffee coffee = this.getCoffee(id);
        coffee.setMenge(menge);
        coffee.setName(name);
        this.entityManager.getTransaction().begin();
        this.entityManager.merge(coffee);
        this.entityManager.getTransaction().commit();

    }

    public Coffee getCoffee(int id) {

        Coffee coffee = this.entityManager.find(Coffee.class, id);
        this.entityManager.detach(coffee);
        
        return coffee;

    }

    public List<Coffee> getCoffeeList() {

        this.entityManager.getTransaction().begin();
        List<Coffee> coffeeList = this.entityManager.createQuery("SELECT c FROM Coffee c").getResultList();
        this.entityManager.getTransaction().commit();
        
        return coffeeList;

    }

    public void deleteCoffee(int id) {

        this.entityManager.getTransaction().begin();
        Coffee coffee = this.entityManager.find(Coffee.class, id);
        this.entityManager.remove(coffee);
        this.entityManager.getTransaction().commit();
        
    }

}
```
___

## 4. Implementieren der API

### 4.1. Abhängigkeiten in pom.xml einpflegen
	
* Wir benötigen noch eine weitere Abhängikeit, um uns das Arbeiten mit JSON Objekten zu erleichtern. Füge hierfür den folgenden Teil unter ```<dependencies>``` ein.

```xml
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>2.9.9.1</version>
</dependency>
```

### 4.2. Functions für REST-Verhalten erstellen

* Erstelle nun innerhalb des Ordners __teamcaffeine__ die Dateien:
    
    * __*GETCoffee.java*__ 
    * __*PUTCoffee.java*__
    * __*POSTCoffee.java*__
    * __*DELETECoffee.java*__

#### 4.2.1. GET-Coffee Function

```java
package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

import dev.teamcaffeine.model.*;
import dev.teamcaffeine.dao.*;

public class GETCoffee {

    @FunctionName("GETCoffee")
    public HttpResponseMessage getCoffee(
            @HttpTrigger(name = "req", methods = {HttpMethod.GET}, authLevel =                    
        	AuthorizationLevel.ANONYMOUS, route = "coffee/{id=NONE}") HttpRequestMessage<Optional<String>> 
 		request, final ExecutionContext context, @BindingName("id") String id) {

        context.getLogger().info("Aufruf der getCoffee function");
        
        CoffeeDAO coffeeDAO = new CoffeeDAO();

        List<Coffee> coffeeList = coffeeDAO.getCoffeeList();

        if( id.equals("NONE")) {

            return request.createResponseBuilder(HttpStatus.OK).header("Content-Type", "application/json").body(coffeeList).build();

        } else {

            try {

                Coffee coffee = coffeeDAO.getCoffee(Integer.parseInt(id));

                return request.createResponseBuilder(HttpStatus.OK).header("Content-Type", "application/json").body(coffee).build();

            } catch(Exception e) {

                e.printStackTrace();

                return request.createResponseBuilder(HttpStatus.NOT_FOUND).body("No coffee found").build();

            }
           
        }
                
    }

}
```

#### 4.2.2. POST-Coffee Function

```java
package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.teamcaffeine.model.*;
import dev.teamcaffeine.dao.*;

public class POSTCoffee {

    @FunctionName("POSTCoffee")
    public HttpResponseMessage hallofromTeamCaffeine(
            @HttpTrigger(name = "req", methods = {HttpMethod.POST}, authLevel =                    
            AuthorizationLevel.ANONYMOUS, route = "coffee") HttpRequestMessage<Optional<String>> 
        request, final ExecutionContext context) {

        context.getLogger().info("Aufruf der POSTCoffee function");
        
        String requestBody = request.getBody().orElse("");

        try{

            ObjectMapper objectMapper = new ObjectMapper();

            Coffee coffee = objectMapper.readValue(requestBody, Coffee.class);

            CoffeeDAO coffeeDAO = new CoffeeDAO();
            
            coffee = new Coffee(coffee.getName(), coffee.getMenge());

            coffeeDAO.saveCoffee(coffee);

            return request.createResponseBuilder(HttpStatus.CREATED).header("Content-Type", "application/json").body(coffee).build();

        } catch(Exception e) {

            e.printStackTrace();
            return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("Object not well defined").build();

        }
    
    }

}
```

#### 4.2.3. PUT-Coffee Function

```java
package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.teamcaffeine.model.*;
import dev.teamcaffeine.dao.*;

public class PUTCoffee {

    @FunctionName("PUTCoffee")
    public HttpResponseMessage putCoffee(
            @HttpTrigger(name = "req", methods = {HttpMethod.PUT}, authLevel = AuthorizationLevel.ANONYMOUS , 
            route = "coffee/{id:int}") HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context,  @BindingName("id") int id) {

        context.getLogger().info("PUTCoffee funcction");

        CoffeeDAO coffeeDAO = new CoffeeDAO();
        
        if(coffeeDAO.getCoffee(id) == null ) {

            return request.createResponseBuilder(HttpStatus.NOT_FOUND).body("object not found").build();

        } else {

            String requestBody = request.getBody().orElse("");
            context.getLogger().info(requestBody);

            try{

                ObjectMapper objectMapper = new ObjectMapper();

                Coffee coffee = objectMapper.readValue(requestBody, Coffee.class);

                coffeeDAO.updateCoffee(id, coffee.getName(), coffee.getMenge());

                return request.createResponseBuilder(HttpStatus.CREATED).header("Content-Type", "application/json").body(coffeeDAO.getCoffeeList()).build();

            } catch(Exception e) {

                e.printStackTrace();
                return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("Object not well defined").build();

            }
        
        }
        
    }   

}
```

#### 4.2.4. DELETE-Coffee Function

```java
package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

import dev.teamcaffeine.model.*;
import dev.teamcaffeine.dao.*;

public class DELETECoffee {

    @FunctionName("DELETECoffee")
    public HttpResponseMessage deleteCoffee(
            @HttpTrigger(name = "req", methods = {HttpMethod.DELETE}, authLevel = AuthorizationLevel.ANONYMOUS , 
            route = "coffee/{id:int}") HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context,  @BindingName("id") int id) {

        context.getLogger().info("DELETECoffee funcction");

        CoffeeDAO coffeeDAO = new CoffeeDAO();
        
        try {

            coffeeDAO.deleteCoffee(id);

            List<Coffee> coffeeList = coffeeDAO.getCoffeeList();

            return request.createResponseBuilder(HttpStatus.OK).header("Content-Type", "application/json").body(coffeeList).build();

        } catch(Exception e) {

            e.printStackTrace();

            return request.createResponseBuilder(HttpStatus.NOT_FOUND).body("Coffee do not exist").build();

        }
   
    }

}
```

___

## 5. Lokales Testen der API

### 5.1. Angular CLI
	
* Öffne erneut ein Terminal und führe den Befehl __*```npm install -g @angular/cli```*__ aus.

* Bestätige sobald du aufgefordert wirst nach deinen eigenen Präferenzen, indem du __*Yes*__ oder __*No*__ in das Terminal eintippst.

### 5.2. Der Client via Git

* Öffne erneut eine Eingabeaufforderung von Windows.

* Führe ```git clone https://bitbucket.org/teamcaffeineit04/studienprojekt.git``` aus.

* Nun wechsle in das neue Verzeichnis, indem du ```cd studienprojekt\client``` ausführst.

* Über den Befehl ```npm install``` bereitest du den Client vor.

* Tippe nun ```ng serve``` ein und bestätige mit __Enter__.

### 5.3. Aufrufen des Clients

* Starte erneut die Functions mithilfe von __*```mvn clean package azure-functions:run```*__ innerhalb eines Projektterminals.

* Gib ```localhost:4200``` in die Adresszeile deines Browsers ein. 

---

## 6. Azure Deployment

### 6.1. Anmelden mit der Azure CLI

* Um die Function auf Azure zu Deployen müssen wir uns erst über die CLI anmelden. 

* Hierfür wechseln wir wieder in das Terminal von VS Code und geben ```az login``` ein.

* Im sich öffnenden Browser anmelden.

### 6.2. pom.xml bearbeiten

* Wir bearbeiten noch einmal die pom.xml Datei.

* Unter ```<properties>``` ändern wir die Einträge ```<functionAppRegion>``` und ```<functionResourceGroup>``` wie folgt ab
```xml
<functionAppRegion>northeurope</functionAppRegion>
<functionResourceGroup>teamcaffeine</functionResourceGroup>
```

### 6.3. Deployment

* Mithilfe des Terminalbefehls __```mvn clean package azure-functions:deploy```__ deployen wir nun auf Azure.

* Nun können wir unsere Function aus der Cloud aufrufen. Am besten kopierst du dir den Wert unter ```<properties>``` … ```< functionAppName>``` in der pom.xml Datei und fügst __*```.azurewebsites.net/hallo-from-TeamCaffeine```*__ an.

* Du solltest nun den Begrüßungstext der Function in deinem Browser sehen.

### 6.4. CORS im Azure Portal

* Öffne in einem Browser das [Azure Portal](https://portal.azure.com) und melde dich an.

* Gebe in der Suchleiste des Portals den Namen deiner Function ein und wähle sie aus.

* wähle hier zuerst den __*Platform features*__ (__1__)-Reiter aus und klicke anschließend auf __*CORS*__ im Untermenü __*API*__ (__2__) ![Azure Portal Platform features](./README/pictures/azure_portal_1.png)

* Füge nun im darauffolgenden Fenster ein __*__ (__1__) in das leere Feld ein. Als nächstes löscht du nun noch die restlichen Einträge (__2__) und bestätigst deine Einstellungen, indem du auf __Save__ drückst (__3__) 
![Azure Portal Platform features](./README/pictures/azure_portal_cors_1.png)

* Nun kannst du die Applikation über den lokalen Client austesten oder die von uns zur verfügung gestellte [Version](https://teamcaffeineit04.bitbucket.io/coffeelist/) verwenden.

___




