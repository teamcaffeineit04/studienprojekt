package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

import dev.teamcaffeine.model.*;
import dev.teamcaffeine.dao.*;

public class GETCoffee {

    @FunctionName("GETCoffee")
    public HttpResponseMessage getCoffee(
            @HttpTrigger(name = "req", methods = {HttpMethod.GET}, authLevel =                    
        	AuthorizationLevel.ANONYMOUS, route = "coffee/{id=NONE}") HttpRequestMessage<Optional<String>> 
 		request, final ExecutionContext context, @BindingName("id") String id) {

        context.getLogger().info("Aufruf der getCoffee function");
        
        CoffeeDAO coffeeDAO = new CoffeeDAO();

        List<Coffee> coffeeList = coffeeDAO.getCoffeeList();

        if( id.equals("NONE")) {

            return request.createResponseBuilder(HttpStatus.OK).header("Content-Type", "application/json").body(coffeeList).build();

        } else {

            try {

                Coffee coffee = coffeeDAO.getCoffee(Integer.parseInt(id));

                return request.createResponseBuilder(HttpStatus.OK).header("Content-Type", "application/json").body(coffee).build();

            } catch(Exception e) {

                e.printStackTrace();

                return request.createResponseBuilder(HttpStatus.NOT_FOUND).body("No coffee found").build();

            }
           
        }
                
    }

}
