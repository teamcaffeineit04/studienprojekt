package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.teamcaffeine.model.*;
import dev.teamcaffeine.dao.*;

public class PUTCoffee {

    @FunctionName("PUTCoffee")
    public HttpResponseMessage putCoffee(
            @HttpTrigger(name = "req", methods = {HttpMethod.PUT}, authLevel = AuthorizationLevel.ANONYMOUS , 
            route = "coffee/{id:int}") HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context,  @BindingName("id") int id) {

        context.getLogger().info("PUTCoffee funcction");

        CoffeeDAO coffeeDAO = new CoffeeDAO();
        
        if(coffeeDAO.getCoffee(id) == null ) {

            return request.createResponseBuilder(HttpStatus.NOT_FOUND).body("object not found").build();

        } else {

            String requestBody = request.getBody().orElse("");
            context.getLogger().info(requestBody);

            try{

                ObjectMapper objectMapper = new ObjectMapper();

                Coffee coffee = objectMapper.readValue(requestBody, Coffee.class);

                coffeeDAO.updateCoffee(id, coffee.getName(), coffee.getMenge());

                return request.createResponseBuilder(HttpStatus.CREATED).header("Content-Type", "application/json").body(coffeeDAO.getCoffeeList()).build();


            } catch(Exception e) {

                e.printStackTrace();
                return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("Object not well defined").build();

            }
        
        }
        
    }   

}