package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.teamcaffeine.model.*;
import dev.teamcaffeine.dao.*;

public class POSTCoffee {

    @FunctionName("POSTCoffee")
    public HttpResponseMessage hallofromTeamCaffeine(
            @HttpTrigger(name = "req", methods = {HttpMethod.POST}, authLevel =                    
        	AuthorizationLevel.ANONYMOUS, route = "coffee") HttpRequestMessage<Optional<String>> 
 		request, final ExecutionContext context) {

        context.getLogger().info("Aufruf der POSTCoffee function");
        
        String requestBody = request.getBody().orElse("");

        try{

            ObjectMapper objectMapper = new ObjectMapper();

            Coffee coffee = objectMapper.readValue(requestBody, Coffee.class);

            CoffeeDAO coffeeDAO = new CoffeeDAO();
            
            coffee = new Coffee(coffee.getName(), coffee.getMenge());

            coffeeDAO.saveCoffee(coffee);

            return request.createResponseBuilder(HttpStatus.CREATED).header("Content-Type", "application/json").body(coffee).build();


        } catch(Exception e) {

            e.printStackTrace();
            return request.createResponseBuilder(HttpStatus.BAD_REQUEST).body("Object not well defined").build();

        }
    
    }

}
