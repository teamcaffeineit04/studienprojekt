package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

public class HalloTeamCaffeine {

    @FunctionName("hallo-from-TeamCaffeine")
    public HttpResponseMessage hallofromTeamCaffeine(
            @HttpTrigger(name = "req", methods = {HttpMethod.GET}, authLevel =                    
        	AuthorizationLevel.ANONYMOUS) HttpRequestMessage<Optional<String>> 
 		request, final ExecutionContext context) {

        context.getLogger().info("Aufruf der hallo-from-TeamCaffeine function");
        
        return request.createResponseBuilder(HttpStatus.OK).body("Hello! We are TeamCoffeine").build();
    
    }

}
