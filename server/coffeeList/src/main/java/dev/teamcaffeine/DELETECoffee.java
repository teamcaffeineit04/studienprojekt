package dev.teamcaffeine;

import java.util.*;
import com.microsoft.azure.functions.annotation.*;
import com.microsoft.azure.functions.*;

import dev.teamcaffeine.model.*;
import dev.teamcaffeine.dao.*;

public class DELETECoffee {

    @FunctionName("DELETECoffee")
    public HttpResponseMessage deleteCoffee(
            @HttpTrigger(name = "req", methods = {HttpMethod.DELETE}, authLevel = AuthorizationLevel.ANONYMOUS , 
            route = "coffee/{id:int}") HttpRequestMessage<Optional<String>> request,
            final ExecutionContext context,  @BindingName("id") int id) {

        context.getLogger().info("DELETECoffee funcction");

        CoffeeDAO coffeeDAO = new CoffeeDAO();
        
        try {

            coffeeDAO.deleteCoffee(id);

            List<Coffee> coffeeList = coffeeDAO.getCoffeeList();

            return request.createResponseBuilder(HttpStatus.OK).header("Content-Type", "application/json").body(coffeeList).build();

        } catch(Exception e) {

            e.printStackTrace();

            return request.createResponseBuilder(HttpStatus.NOT_FOUND).body("Coffee do not exist").build();

        }
   
    }

}