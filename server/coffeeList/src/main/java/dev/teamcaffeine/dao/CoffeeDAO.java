package dev.teamcaffeine.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dev.teamcaffeine.model.Coffee;

public class CoffeeDAO {

    private EntityManager entityManager;

    public CoffeeDAO() {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("coffee-production-MSSQL");
        this.entityManager = entityManagerFactory.createEntityManager();
        
    }

    public void saveCoffee(Coffee coffee) {

        this.entityManager.getTransaction().begin();
        this.entityManager.persist(coffee);
        this.entityManager.getTransaction().commit();

    }

    public void updateCoffee(int id, String name, int menge) {

        Coffee coffee = this.getCoffee(id);
        coffee.setMenge(menge);
        coffee.setName(name);
        this.entityManager.getTransaction().begin();
        this.entityManager.merge(coffee);
        this.entityManager.getTransaction().commit();

    }

    public Coffee getCoffee(int id) {

        Coffee coffee = this.entityManager.find(Coffee.class, id);
        this.entityManager.detach(coffee);
        
        return coffee;

    }

    public List<Coffee> getCoffeeList() {

        this.entityManager.getTransaction().begin();
        List<Coffee> coffeeList = this.entityManager.createQuery("SELECT c FROM Coffee c").getResultList();
        this.entityManager.getTransaction().commit();
        
        return coffeeList;

    }

    public void deleteCoffee(int id) {

        this.entityManager.getTransaction().begin();
        Coffee coffee = this.entityManager.find(Coffee.class, id);
        this.entityManager.remove(coffee);
        this.entityManager.getTransaction().commit();
        
    }

}